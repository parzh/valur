**IMPORTANT**: The codebase for Valuer has been moved to [Valuer/valuer](https://gitlab.com/valuer/main). Please visit the page to see the recent code.

***

**Valur**<sup>([@npm](https://npmjs.org/package/valur))</sup> is an advanced declarative value validator.

***

## Installation:

```cmd
$ npm install valur --save
```

## Usage:

_(see #18)_

To validate a value before its use, just preform validation a couple of lines before.

```ts
const root = (radicand: number, degree: number): number =>
    radicand ** degree ** -1;

root(8, 3);
// 2 (no error)

root(-8, 3);
// NaN (no error, but should be)

// ***

const rootValidated = (radicand: number, degree: number): number => {
    // roots of negative numbers do not make sense in JavaScript
    // therefore radicand cannot be negative
    valur(radicand, "radicand").as({ spectrum: "non-negative" });

    return root(radicand, degree);
};

rootValidated(-8, 3);
// Error: Validation failed: radicand is a negative number: value <number> -8, validator "spectrum"
```