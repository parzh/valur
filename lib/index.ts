import { FunctionSpecific, Thunk, Thrower, TypeOf, Class, NumericRange } from "@valuer/types";
import { brackets } from "@valuer/brackets";
import { help } from "@valuer/help";
import { fn } from "@valuer/fn";
import { is } from "@valuer/is";

import { panic } from "./panic";

type Role = string;
type Failure = string;

type ValueKindGeneral = "void" | "non-void" | "defined" | "non-null";
type ValueKindPrimitive = "primitive" | "primitive-non-void" | "primitive-defined" | "primitive-non-null";
type ValueKindComposite = "composite" | "composite-non-void" | "composite-defined" | "composite-non-null";

type ValueKind = "any" | ValueKindGeneral | ValueKindPrimitive | ValueKindComposite;

type NumberKindIntegerAmbiguous = "integer" | "integer-even" | "integer-odd";
type NumberKindIntegerSafe = "integer-safe" | "integer-even-safe" | "integer-odd-safe";
type NumberKindInteger = NumberKindIntegerAmbiguous | NumberKindIntegerSafe;
type NumberKindNonInteger = "non-integer" | "non-integer-finite";

type NumberKind = "any" | "numeric" | "finite" | NumberKindNonInteger | NumberKindInteger;

type NumberSpectrum = "any" | "positive" | "negative" | "non-positive" | "non-negative" | "non-zero";

// ***

type DescriptorBase<
	Value = any,
> = {
	/** Expected value */
	value: Value;

	/** Set of allowed values */
	set: Value[];

	/** Kind of value */
	kind: ValueKind;

	// ***

	/** Expected result of applying `typeof` to the value */
	typeOf: TypeOf;

	/** Expected value's constructor */
	instanceOf: Class<Value>;

	// ***

	/** Type of numeric value */
	number: NumberKind;

	/** Allowed part of the number spectrum */
	spectrum: NumberSpectrum;

	/** The smallest and the largest possible numeric values respectively */
	range: NumericRange;

	// ***

	/** Pattern to match against the string */
	pattern: RegExp;

	/** Length of the string */
	length: number;

	/** Min and max length of the string respectively */
	lengthRange: NumericRange;
};

type DescriptorIncoming<
	Value = any,
> = Partial<DescriptorBase<Value>> & {
	[failure: string]: any;
};

// ***

type Validator = keyof DescriptorBase;
type ValidatorFailure = string;

type ValidatorWithoutDefaultValidation = Validator & ("value" | "set" | "typeOf" | "instanceOf" | "length");
type ValidatorWithDefaultValidation = Exclude<Validator, ValidatorWithoutDefaultValidation>;

type ValidatorString = Validator & ("typeOf" | "number" | "spectrum" | "kind");
type ValidatorStringCustom = Exclude<ValidatorString, "typeOf">;
type ValidatorNonString = Exclude<Validator, ValidatorString>;

// ***

type Validation<
	Value = any,
	CorrespondingValidator extends Validator = Validator,
> = DescriptorBase<Value>[CorrespondingValidator];

type ValidationString<
	CorrespondingValidatorString extends ValidatorString = ValidatorString,
> = DescriptorBase<string>[CorrespondingValidatorString];

type ValidationSole<
	Value = any,
> = ValidationString | Thunk<Value> | Validation<Value, "pattern" | "set" | "value">;

// ***

type ConfigBase = {
	/** Validation behavior */
	mode: "assert" | "check" | "describe" | "switch";

	/** Strictness of "check" mode */
	checkStrict: boolean;

	/** Error message detalization */
	details: "none" | "message" | "value" | "validator" | "validation" | "all";

	/** Inclusiveness of `range` validation */
	rangeInclusive: [ boolean, boolean ];

	/** Inclusiveness of `lengthRange` validation */
	lengthRangeInclusive: [ boolean, boolean ];
};

type ConfigIncoming = Partial<ConfigBase>;

// ***

type Modifier = keyof ConfigBase;
type ModifierFailure = string;

type ModifierString = Modifier & ("mode" | "details");
type ModifierNonString = Exclude<Modifier, ModifierString>;

// ***

type Modification<
	CorrespondingModifier extends Modifier = Modifier,
> = ConfigBase[CorrespondingModifier];

// ***

type Processor<
	ArgumentType = any,
> = {
	name: Validator | Failure;
	thunk: Thunk<ArgumentType>;
	failure: Failure;
	validator: Validator | "(custom)";
	validation: Validation;
};

type Switcher<
	Value = any,
> = {
	name: string;
	value: Value;
};

// ***

const DEFAULT_FAILURE = "does not meet the constraint";

const VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP: {
	[V in ValidatorString]: ValidationString<V>[];
} = {
	kind: [
		"any", "composite", "primitive", "void",
		"primitive-non-void", "composite-non-void", "non-void",
		"primitive-defined", "composite-defined", "defined",
		"primitive-non-null", "composite-non-null", "non-null"
	],

	typeOf: [
		"string", "number", "boolean", "symbol", "undefined", "object", "function"
	],

	number: [
		"any", "numeric", "finite", "non-integer", "non-integer-finite",
		"integer", "integer-even", "integer-odd",
		"integer-safe", "integer-even-safe", "integer-odd-safe",
	],

	spectrum: [
		"any", "positive", "negative", "non-positive", "non-negative", "non-zero"
	],
};

const VALIDATOR_TO_META_VALIDATOR_MAP: {
	[V in ValidatorString]: Thrower<ValidationString<V>>;
} & {
	[V in ValidatorNonString]: Thrower<Validation<any, V>>;
} = {
	value: () => null, // no constraints
	set: getMetaValidatorNonString("set", is.array(), "is not an array"),
	kind: getMetaValidatorString("kind"),

	typeOf: getMetaValidatorString("typeOf"),
	instanceOf: getMetaValidatorNonString("instanceOf", is._function, "is not a class"),

	number: getMetaValidatorString("number"),
	spectrum: getMetaValidatorString("spectrum"),
	range: getMetaValidatorNonString("range", is.array(2, is.numeric()), "is invalid range"),

	pattern: getMetaValidatorNonString("pattern", is.instanceOf(RegExp), "is not a regular expression"),
	length: getMetaValidatorNonString("length", is.natural(), "is not a natural number"),
	lengthRange: getMetaValidatorNonString("lengthRange", is.array(2, is.natural()), "is invalid length range"),
};

const VALIDATOR_TO_DEFAULT_VALIDATION_MAP: Readonly<{
	[V in ValidatorWithDefaultValidation]: NonNullable<Validation<any, V>>;
} & {
	[V in ValidatorWithoutDefaultValidation]: undefined;
}> = Object.freeze({
	value: undefined,
	set: undefined,
	kind: <ValueKind> "any",

	typeOf: undefined,
	instanceOf: undefined,

	number: <NumberKind> "any",
	spectrum: <NumberSpectrum> "any",
	range: <NumericRange> Object.freeze([-Infinity, Infinity]),

	pattern: /(?:)/,
	length: undefined,
	lengthRange: <NumericRange> Object.freeze([0, Infinity]),
});

const VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP: {
	[V in ValidatorStringCustom]: {
		[Vn in ValidationString<V>]: Failure;
	};
} = {
	kind: {
		"any": null, // no failure possible

		"void": "is not a void",
		"defined": "is undefined",
		"non-null": "is null",
		"non-void": "is a void",

		"primitive": "is not a primitive",
		"primitive-defined": "is undefined or not a primitive",
		"primitive-non-null": "is null or not a primitive",
		"primitive-non-void": "is an object or void",

		"composite": "is a primitive",
		"composite-defined": "is undefined or a primitive",
		"composite-non-null": "is null or a primitive",
		"composite-non-void": "is a primitive or void",
	},

	number: {
		"any": "is not a number",

		"numeric": "is NaN",
		"finite": "is not a finite number",
		"non-integer": "is an integer",
		"non-integer-finite": "is an integer or infinite",

		"integer": "is not an integer",
		"integer-even": "is not an even integer",
		"integer-odd": "is not an odd integer",

		"integer-safe": "is not a safe integer",
		"integer-even-safe": "is not a safe even integer",
		"integer-odd-safe": "is not a safe odd integer",
	},

	spectrum: {
		"any": "is not a number",

		"negative": "is not a negative number",
		"positive": "is not a positive number",
		"non-positive": "is a positive number",
		"non-negative": "is a negative number",
		"non-zero": "is zero",
	},
};

const VALIDATOR_TO_FAILURE_FACTORY_MAP: {
	[V in Validator]: FunctionSpecific<Validation<any, V>, Failure>;
} = {
	value: (value) =>
		`is not ${ help.getPrintable(value) }`,

	set: () =>
		`was not found in the set`,

	kind: (kind) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP.kind[kind],

	// ***

	typeOf: (type) =>
		`is not of type "${ type }"`,

	instanceOf: (klass) =>
		`is not an instance of ${ klass.name }`,

	// ***

	number: (number) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP.number[number],

	spectrum: (spectrum) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP.spectrum[spectrum],

	range: (range) =>
		`is out of bounds "${ help.getPrintableRange(range) }"`,

	// ***

	pattern: (pattern) =>
		`does not match the pattern "${ pattern }"`,

	length: (length) =>
		`is not ${ length } character${ length === 1? '' : 's' } long`,

	lengthRange: (lengthRange) =>
		`is not ${ help.getPrintableRange(lengthRange) } characters long`,
};

const VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP: {
	[V in ValidatorStringCustom]: {
		[Vn in ValidationString<V>]: Thunk;
	};
} = {
	kind: {
		"any": is._any,

		"void": is._void,
		"defined": fn.not(is._undefined),
		"non-null": fn.not(is._null),
		"non-void": is._nonVoid,

		"primitive": is._primitive,
		"primitive-defined": fn.any(is._nonVoidPrimitive, is._null),
		"primitive-non-null": fn.any(is._nonVoidPrimitive, is._undefined),
		"primitive-non-void": is._nonVoidPrimitive,

		"composite": is._composite,
		"composite-defined": fn.any(is._nonVoidComposite, is._null),
		"composite-non-null": fn.any(is._nonVoidComposite, is._undefined),
		"composite-non-void": is._nonVoidComposite,
	},

	number: {
		"any": is._number,

		"finite": fn.all(is._number, Number.isFinite),
		"numeric": fn.all(is._number, is.numeric()),
		"non-integer": fn.all(is._number, is.float()),
		"non-integer-finite": fn.all(is._number, Number.isFinite, is.float()),

		"integer": Number.isInteger,
		"integer-even": is.integer("even"),
		"integer-odd": is.integer("odd"),

		"integer-safe": Number.isSafeInteger,
		"integer-even-safe": fn.all(Number.isSafeInteger, is.integer("even")),
		"integer-odd-safe": fn.all(Number.isSafeInteger, is.integer("odd")),
	},

	spectrum: {
		"any": is._number,

		"negative": value => value < 0,
		"positive": value => value > 0,
		"non-positive": value => value <= 0,
		"non-negative": value => value >= 0,
		"non-zero": value => value !== 0,
	},
};

const VALIDATOR_TO_THUNK_FACTORY_MAP: {
	[V in ValidatorString]: FunctionSpecific<ValidationString<V>, Thunk>;
} & {
	[V in ValidatorNonString]: FunctionSpecific<Validation<any, V>, Thunk>;
} = {
	value: (expected) => (actual) =>
		actual === expected,

	set: (set) => (input) =>
		set.includes(input),

	kind: (kind) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP.kind[kind],

	// ***

	typeOf: is.ofType,
	instanceOf: is.instanceOf,

	// ***

	number: (number) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP.number[number],

	spectrum: (spectrum) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP.spectrum[spectrum],

	range: is.inRange,

	// ***

	pattern: (pattern) => (input) =>
		is._string(input) && pattern.test(input),

	length: (length) => (input) =>
		is._string(input) && input.length === length,

	lengthRange: (lengthRange) => (input) =>
		is._string(input) && is.inRange(lengthRange)(input.length),
};

const MODIFIER_TO_DEFAULT_MODIFICATION_MAP: ConfigBase = {
	mode: "assert",
	checkStrict: true,
	details: "validator",
	rangeInclusive: [ true, true ],
	lengthRangeInclusive: [ true, true ],
};

const MODIFIER_STRING_TO_MODIFICATION_LIST_MAP: {
	[M in ModifierString]: Modification<M>[];
} = {
	mode: [ "assert", "check", "describe", "switch" ],
	details: [ "none", "message", "value", "validator", "validation", "all" ],
};

const MODIFIER_TO_MODIFIER_VALIDATOR_MAP: {
	[M in Modifier]: Thrower<Modification<M>>;
} = {
	mode: getModifierValidatorString("mode"),
	checkStrict: getModifierValidatorNonString("checkStrict", is._boolean, "is not a boolean"),
	details: getModifierValidatorString("details"),
	rangeInclusive:
		getModifierValidatorNonString("rangeInclusive", is.array(2, is._boolean), "is not an array of two booleans"),
	lengthRangeInclusive:
		getModifierValidatorNonString("lengthRangeInclusive", is.array(2, is._boolean), "is not an array of two booleans"),
};

// ***

function isValidator(value: string): value is Validator {
	return value in VALIDATOR_TO_DEFAULT_VALIDATION_MAP;
}

function isValidationString<V extends ValidatorString = ValidatorString>(
	validation: string,
	validator?: V,
): validation is ValidationString<V> {
	const map = VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP;

	if (validator != null) {
		const list: ValidationString<V>[] = map[validator];

		return list.includes(validation as any);
	}

	else for (const _validator in map)
		if (isValidationString(validation, _validator as V))
			return true;

	return false;
}

function isThunk<Value = any>(value: any): value is Thunk<Value> {
	return is._function(value) /* additional verifications are probably required */;
}

function metaValidate<V extends Validator = Validator>(validator: V, validation: Validation<any, V>) {
	const metaValidator: Thrower<Validation<any, V>, void> = VALIDATOR_TO_META_VALIDATOR_MAP[validator];

	return metaValidator(validation);
}

function getMetaValidatorString<V extends ValidatorString = ValidatorString>(validator: V): Thrower<ValidationString<V>> {
	const validations: ValidationString<V>[] = VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP[validator];

	return (validation) => {
		if (!validations.includes(validation))
			return panic.now(
				"Invalid validator",
				`"${ validator }" is invalid string`,
				`validation ${ help.getPrintable(validation) }`,
			);
	};
}

function getMetaValidatorNonString<V extends Validator = Validator>(
	validator: V,
	thunk: Thunk<Validation<any, V>>,
	failure: ValidatorFailure,
): Thrower<Validation<any, V>> {
	return (validation) => {
		if (!thunk(validation))
			return panic.now(
				"Invalid validator",
				`"${ validator }" ${ failure }`,
				`validation ${ help.getPrintable(validation) }`,
			);
	};
}

function getFailure(validator: string, validation: Validation): Failure {
	if (validator === "custom")
		return DEFAULT_FAILURE;

	else if (!isValidator(validator))
		return String(validator) as Failure;

	const failureFactory: FunctionSpecific<Validation, Failure> = VALIDATOR_TO_FAILURE_FACTORY_MAP[validator];

	return failureFactory(validation);
}

function getThunk<Value = any>(validator: string, validation: Validation<Value>): Thunk<Value> {
	if (!isValidator(validator))
		return null;

	const thunkFactory: FunctionSpecific<Validation, Thunk> = VALIDATOR_TO_THUNK_FACTORY_MAP[validator];

	return thunkFactory(validation);
}

function isModifier(value: any): value is Modifier {
	return value in MODIFIER_TO_DEFAULT_MODIFICATION_MAP;
}

function getModifierValidatorString<M extends ModifierString = ModifierString>(modifier: M): Thrower<Modification<M>> {
	const modifications: Modification<M>[] = MODIFIER_STRING_TO_MODIFICATION_LIST_MAP[modifier];

	return (modification) => {
		if (!modifications.includes(modification))
			return panic.now(
				"Invalid config",
				`"${ modifier }" is invalid string`,
				`modification ${ help.getPrintable(modification) }`,
			);
	};
}

function getModifierValidatorNonString<M extends ModifierNonString = ModifierNonString>(
	modifier: M,
	thunk: Thunk<Modification<M>>,
	failure: ModifierFailure,
): Thrower<Modification<M>> {
	return (modification) => {
		if (!thunk(modification))
			return panic.now(
				"Invalid config",
				`"${ modifier }" ${ failure }`,
				`modification ${ help.getPrintable(modification) }`,
			);
	};
}

function validateModifier<M extends Modifier = Modifier>(modifier: M, modification: Modification<M>) {
	const modifierValidator: Thrower = MODIFIER_TO_MODIFIER_VALIDATOR_MAP[modifier];

	return modifierValidator(modification);
}

function isDescriptorFlow(...args: any[]): boolean {
	return is.inRange([1, 2])(args.length) && args.every(is.directInstanceOf(Object));
}

function getConfig(config: ConfigIncoming): ConfigBase {
	const _config: ConfigIncoming = Object(config);

	for (const modifier in _config)
		if (isModifier(modifier))
			validateModifier(modifier, _config[modifier]);

		else return panic.now(
			"Invalid config",
			`modifier "${ modifier }" is unknown`,
		);

	return Object.assign({}, MODIFIER_TO_DEFAULT_MODIFICATION_MAP, _config);
}

function getHooks<Value = any>(config: ConfigBase): {
	["validation.failure"](processor?: Processor<Value>, value?: Value, role?: Role): any;
	["validation.success"](processor?: Processor<Value>): any;
	["validation.continue"](): boolean;
	["validation.ending"](value?: Value, role?: Role): any;
} {
	const assist = {
		check: <boolean[]> [],
		describe: <Record<Validator, true | Failure>> {},
		switch: <Switcher<Value>> Object.create(null),
	};

	if (config.mode === "assert")
		return ({
			"validation.failure": (processor, value, role) =>
				panic.now(
					"Validation failed",
					`${ role } ${ processor.failure }`,
					`value ${ help.getPrintable(value) }`,
					`validator "${ processor.validator }"`,
					`validation ${ help.getPrintable(processor.validation) }`,
				),

			"validation.success": (processor?) => { /* skip */ },

			"validation.continue": () => true,

			"validation.ending": (value) => value,
		});

	else if (config.mode === "check")
		return ({
			"validation.failure": (processor?, value?, role?) =>
				assist.check.push(false),

			"validation.success": (processor?) =>
				assist.check.push(true),

			"validation.continue": (): boolean => {
				if (config.checkStrict)
					// continue if no `false` values were produced
					return !assist.check.some(is.boolean(false));
	
				// continue if no `true` values were produced
				else return !assist.check.some(is.boolean(true));
			},

			"validation.ending": (value?) => {
				if (config.checkStrict)
					// return `true` if all validations were successful
					return assist.check.every(is.boolean(true));
	
				// return `true` if any validation was successful
				else return assist.check.some(is.boolean(true));
			},
		});

	else if (config.mode === "describe")
		return ({
			"validation.failure": (processor, value?, role?) => {
				if (processor.validator !== "(custom)")
					assist.describe[processor.validator] = `${ role } ${ processor.failure }`;
			},

			"validation.success": (processor?) => {
				if (processor.validator !== "(custom)")
					assist.describe[processor.validator] = true;
			},

			"validation.continue": () => true,

			"validation.ending": (value?) => assist.describe,
		})

	else if (config.mode === "switch")
		return ({
			"validation.failure": (processor, value, role?) =>
				assist.switch = { name: processor.name, value },

			"validation.success": (processor?) => { /* skip */ },

			"validation.continue": () => !("name" in assist.switch),

			"validation.ending": (value?) => assist.switch,
		});

	else validateModifier("mode", config.mode);
}

// ***

function core<Value = any>(descriptor: DescriptorIncoming<Value>, config?: ConfigIncoming): (value: Value, role?: Role) => any;

function core<Value = any>(...validations: ValidationSole<Value>[]): (value: Value, role?: Role) => any;

function core<Value = any>(...args: any[]) {
	const usingDescriptor = isDescriptorFlow(args);
	const config = getConfig(usingDescriptor? args[1] : {});
	const hooks = getHooks<Value>(config);
	const processors = getProcessors<Value>(usingDescriptor, args);

	return function (value: Value, role: Role = "value") {
		for (const processor of processors) {
			if (!processor.thunk(value))
				hooks["validation.failure"](processor, value, role);

			else hooks["validation.success"](processor);

			if (!hooks["validation.continue"]())
				break;
		}

		return hooks["validation.ending"](value);
	};
}

function getProcessors<Value = any>(usingDescriptor: boolean, args: any[]): Processor<Value>[] {
	if (usingDescriptor)
		return getProcessorsUsingDescriptor(args[0] as DescriptorIncoming<Value>, args[1] as ConfigIncoming);

	else return getProcessorsUsingValidations(args as ValidationSole<Value>[]);
}

// ***

function getProcessorsUsingDescriptor<Value = any>(descriptor: DescriptorIncoming<Value>, modifiers?: ConfigIncoming) {
	const processors: Processor<Value>[] = [];

	for (const [validator, validation] of Object.entries(descriptor)) {
		const processor: Processor<Value> = {
			name: validator,
			validation,
			validator: null,
			thunk: null,
			failure: getFailure(validator, validation),
		};

		if (isValidator(validator)) {
			metaValidate(validator, validation);

			processor.validator = validator;
			processor.thunk = getThunk<Value>(validator, validation);
		}

		else {
			processor.validator = "(custom)";

			if (isThunk(validation))
				processor.thunk = validation;

			else return panic.now(
				"Invalid validator",
				`"(custom)" is not a thunk`,
				`validation ${ help.getPrintable(validation) }`,
			);
		}

		if (processor.validator == null || processor.thunk == null)
			return panic.now("Unexpected error", "invalid descriptor");

		processors.push(processor);
	}

	return processors;
}

function getProcessorsUsingValidations<Value = any>(validations: ValidationSole<Value>[]) {
	const descriptor: DescriptorIncoming<Value> = {};
	const thunks: Thunk[] = [];

	const addToSet = (validation: Value | Value[]) => {
		if (!(descriptor.set instanceof Array))
			descriptor.set = [];

		if (validation instanceof Array)
			descriptor.set.push(...validation);

		else descriptor.set.push(validation);
	};

	for (const validation of validations)
		if (is._string(validation)) {
			const map = VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP;
			const validators: ValidatorString[] = [];

			for (const validator in map)
				if (isValidationString(validation, validator as ValidatorString))
					validators.push(validator as ValidatorString);

			if (validators.length)
				for (const validator of validators)
					descriptor[validator] = validation as ValidationString;

			else addToSet(validation as never);
		}

		else if (validation instanceof RegExp)
			descriptor.pattern = validation;

		else if (isThunk<Value>(validation))
			thunks.push(validation);

		else addToSet(validation);

	if (thunks.length)
		if (thunks.length === 1)
			descriptor[DEFAULT_FAILURE] = thunks[0];

		else for (const [ index, thunk ] of Array.from(thunks.entries()))
			descriptor[DEFAULT_FAILURE + ' ' + brackets.surround(index, brackets.collection.square.tight)] = thunk;

	return getProcessorsUsingDescriptor(descriptor);
}

// ***

type Delegator = {
	<Value = any>(value: Value, role?: Role): {
		as(descriptor: DescriptorIncoming<Value>, config?: ConfigIncoming): Value;
		as(...validations: ValidationSole<Value>[]): Value;
	};
};

/** @private */
const delegator: Delegator = (value, role?) => ({
	as: (...args: any[]) =>
		core(...args)(value, role),
});

export const valur = Object.assign(delegator, { as: core });
