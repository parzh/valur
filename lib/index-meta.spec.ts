import "mocha";
import { expect } from "chai";
import { valur } from "./index";

const reInvalid = /^Invalid validator:/;

describe('Descriptor for values with unspecified type', () => {
	const junk: any[] = [ 42, "text", /regex/, {}, [], (() => {}) ];

	describe('Validator "value"', () => {
		it("can be anything", () => {
			for (const value of junk)
				expect(() => valur.as(<any> { value })).to.not.throw();
		});
	});

	describe('Validator "set"', () => {
		it("should be an arbitrary array", function() {
			for (const set of [ [], Array(), Array.from(Object()), "text".split("") ])
				expect(() => valur.as({ set })).to.not.throw();

			for (const set of [ 42, "text", /regex/, {}, (() => {}), new Set, arguments ])
				expect(() => valur.as(<any> { set })).to.throw(reInvalid);
		});
	});

	describe('Validator "kind"', () => {
		it("should be a valid description of a value kind", () => {
			const valid = [
				"any",
				"primitive", "composite", "void",
				"primitive-non-void", "composite-non-void", "non-void",
				"primitive-defined", "composite-defined", "defined",
				"primitive-non-null", "composite-non-null", "non-null",
			];

			const legacy = [
				"object", "not-null-object", "non-null-object", "non-null-composite", "non-void-object", "non-void-composite", "defined-object", "defined-composite", "not-null-primitive", "non-null-primitive", "non-void-primitive", "defined-primitive", "not-null",
			];

			for (const kind of valid)
				expect(() => valur.as(<any> { kind })).to.not.throw();

			for (const kind of [ ...junk, ...legacy ])
				expect(() => valur.as(<any> { kind })).to.throw(reInvalid);
		});
	});

	describe('Validator "typeOf"', () => {
		it("should be a name of a primitive type", () => {
			for (const typeOf of [ "string", "number", "boolean", "symbol", "undefined", "object", "function" ])
				expect(() => valur.as(<any> { typeOf })).to.not.throw();

			for (const typeOf of junk)
				expect(() => valur.as(<any> { typeOf })).to.throw(reInvalid);
		});
	});

	describe('Validator "instanceOf"', () => {
		it("should be a class", () => {
			for (const instanceOf of [ Object, String, Number, Array, RegExp ])
				expect(() => valur.as({ instanceOf })).to.not.throw();

			for (const instanceOf of [ 42, "text", /regex/, {}, [] ])
				expect(() => valur.as(<any> { instanceOf })).to.throw(reInvalid);
		});

		it("cannot be a plain function");
	});

	describe('Custom validators', () => {
		it("should be thunks", () => {
			const _junk: any = [ 42, "text", /regex/, { "is number": () => false }, { "is number": false }, { "is": 17 }, [] ];

			for (const thunk of [ () => true, () => false ])
				expect(() => valur.as(<any> { custom: thunk })).to.not.throw();

			for (const object of _junk)
				expect(() => valur.as(<any> { custom: object })).to.throw(reInvalid);
		});

		it("can have custom failure messages", () => {
			for (const message of [ "is invalid", "is not positive", "has no 'name' property" ])
				expect(() => valur.as({ [message]: () => true })).to.not.throw();
		});
	});
});

describe('Descriptor for numbers', () => {
	describe('Validator "number"', () => {
		it("should be a valid class of numbers", () => {
			const valid = [
				"any", "numeric", "finite", "non-integer", "non-integer-finite",
				"integer", "integer-even", "integer-odd",
				"integer-safe", "integer-even-safe", "integer-odd-safe",
			];
			const legacy = [ "float only", "odd", "even", "integer-saf" ];

			for (const number of valid)
				expect(() => valur.as(<any> { number })).to.not.throw();

			for (const number of [ "", "evan", "positive", ...legacy ])
				expect(() => valur.as(<any> { number })).to.throw(reInvalid);
		});
	});

	describe('Validator "spectrum"', () => {
		it("should be a part of number spectrum", () => {
			for (const spectrum of [ "any", "non-zero", "positive", "negative", "non-positive", "non-negative" ])
				expect(() => valur.as(<any> { spectrum })).to.not.throw();

			for (const spectrum of [ "", "integer", "finite" ])
				expect(() => valur.as(<any> { spectrum })).to.throw(reInvalid);
		});
	});

	describe('Validator "range"', () => {
		it("should be a pair of numbers except NaN", () => {
			for (const range of [
				[0, 1],
				[1, 0],
				[0, Infinity],
				[Infinity, 0],
				[-Infinity, Infinity]
			])
				expect(() => valur.as(<any> { range })).to.not.throw();

			for (const range of [
				[NaN, Infinity],
				[NaN, NaN],
				[0]
			])
				expect(() => valur.as(<any> { range })).to.throw(reInvalid);
		});
	});
});

describe('Descriptor for strings', () => {
	describe('Validator "pattern"', () => {
		it("should be an instance of RegExp", () => {
			for (const pattern of [ /regex/, /hello/g, new RegExp(".*", "m") ])
				expect(() => valur.as({ pattern })).to.not.throw();

			for (const pattern of [ "/regex/", 15 ])
				expect(() => valur.as(<any> { pattern })).to.throw(reInvalid);
		});
	});

	describe('Validator "length"', () => {
		it("should be a natural number", () => {
			const zeroes = Array(10).fill(0);
			const floats = zeroes.map(() => Math.random() * (42 - 17) + 17);
			const naturals = floats.map(Math.round);

			for (const length of naturals)
				expect(() => valur.as({ length })).to.not.throw();

			for (const length of floats)
				expect(() => valur.as({ length })).to.throw(reInvalid);
		});
	});

	describe('Validator "lengthRange"', () => {
		it("should be a pair of natural numbers", () => {
			for (const lengthRange of [
				[42, 17],
				[17, 42],
				[0, 42],
			])
				expect(() => valur.as(<any> { lengthRange })).to.not.throw();

			for (const lengthRange of [
				[0, Infinity],
				[NaN, Infinity],
				[NaN, NaN],
				[-1, 15]
			])
				expect(() => valur.as(<any> { lengthRange })).to.throw(reInvalid);
		});
	});
});
