import "mocha";
import { expect } from "chai";
import { valur } from "./index";

const getBothForms = <T>(value: T, descriptor: object): [Function, Function] => [
	() => valur(value).as(descriptor),
	() => valur.as(descriptor)(value),
];

const reInvalid = /Validation failed:/;

describe('Simple validations', () => {
	describe('validator "value"', () => {
		it("should validate particular value", () => {
			for (const form of getBothForms(42, { value: 42 }))
				expect(form).to.not.throw();

			for (const form of getBothForms(17, { value: 17 }))
				expect(form).to.not.throw();

			for (const form of getBothForms({}, { value: {} }))
				expect(form).to.throw(reInvalid);
		});
	});

	describe('validator "set"', () => {
		it("should validate particular set of values", () => {
			const values = [ 42, 17, 3.5, null, NaN, {} ];

			for (const form of getBothForms(42, { set: values }))
				expect(form).to.not.throw();

			for (const form of getBothForms(NaN, { set: values }))
				expect(form).to.not.throw();

			for (const form of getBothForms({}, { set: values }))
				expect(form).to.throw(reInvalid);

			const existing = values[Math.floor(Math.random() * values.length)];

			for (const form of getBothForms(existing, { set: values }))
				expect(form).to.not.throw();
		});
	});

	describe('validator "kind"', () => {
		const nonVoidPrims: any[] = [ 42, 17, "text", NaN, Infinity ];
		const nonVoidObjs: any[] = [ {}, (() => {}), /regex/ ];
		const nils: any[] = [ null, "a".match(/b/) ];
		const undefs: any[] = [ undefined, void 0 ];

		// any
		it('should validate anything', () => {
			for (const value of [ ...nonVoidPrims, ...nonVoidObjs, ...nils, ...undefs ])
				for (const form of getBothForms(<any> value, { kind: "any" }))
					expect(form).to.not.throw();
		});

		// void
		it('should validate voids', () => {
			for (const value of [ ...nils, ...undefs ])
				for (const form of getBothForms(<any> value, { kind: "void" }))
					expect(form).to.not.throw();

			for (const value of [ ...nonVoidPrims, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "void" }))
					expect(form).to.throw(reInvalid);
		});

		// non-null
		it('should validate anything except `null`', () => {
			for (const value of [ ...undefs, ...nonVoidPrims, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "non-null" }))
					expect(form).to.not.throw();

			for (const value of [ ...nils ])
				for (const form of getBothForms(<any> value, { kind: "non-null" }))
					expect(form).to.throw(reInvalid);
		});

		// non-void
		it('should validate non-void values', () => {
			for (const value of [ ...nonVoidPrims, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "non-void" }))
					expect(form).to.not.throw();

			for (const value of [ ...nils, ...undefs ])
				for (const form of getBothForms(<any> value, { kind: "non-void" }))
					expect(form).to.throw(reInvalid);
		});

		// primitive
		it('should validate primitive values', () => {
			for (const value of [ ...nils, ...undefs, ...nonVoidPrims ])
				for (const form of getBothForms(<any> value, { kind: "primitive" }))
					expect(form).to.not.throw();

			for (const value of [ ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "primitive" }))
					expect(form).to.throw(reInvalid);
		});

		// primitive-defined
		it('should validate all primitive values except `undefined`', () => {
			for (const value of [ ...nils, ...nonVoidPrims ])
				for (const form of getBothForms(<any> value, { kind: "primitive-defined" }))
					expect(form).to.not.throw();

			for (const value of [ ...undefs, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "primitive-defined" }))
					expect(form).to.throw(reInvalid);
		});

		// primitive-non-null
		it('should validate all primitive values except `null`', () => {
			for (const value of [ ...undefs, ...nonVoidPrims ])
				for (const form of getBothForms(<any> value, { kind: "primitive-non-null" }))
					expect(form).to.not.throw();

			for (const value of [ ...nils, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "primitive-non-null" }))
					expect(form).to.throw(reInvalid);
		});

		// primitive-non-void
		it('should validate non-void primitive values', () => {
			for (const value of [ ...nonVoidPrims ])
				for (const form of getBothForms(<any> value, { kind: "primitive-non-void" }))
					expect(form).to.not.throw();

			for (const value of [ ...nils, ...undefs, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "primitive-non-void" }))
					expect(form).to.throw(reInvalid);
		});

		// composite
		it('should validate composite values', () => {
			for (const value of [ ...nils, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "composite" }))
					expect(form).to.not.throw();

			for (const value of [ ...undefs, ...nonVoidPrims ])
				for (const form of getBothForms(<any> value, { kind: "composite" }))
					expect(form).to.throw(reInvalid);
		});

		// composite-defined
		it('should validate all composite values or `null`, but not `undefined`', () => {
			for (const value of [ ...nils, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "composite-defined" }))
					expect(form).to.not.throw();

			for (const value of [ ...undefs, ...nonVoidPrims ])
				for (const form of getBothForms(<any> value, { kind: "composite-defined" }))
					expect(form).to.throw(reInvalid);
		});

		// composite-non-null
		it('should validate all composite values except `null`', () => {
			for (const value of [ ...undefs, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "composite-non-null" }))
					expect(form).to.not.throw();

			for (const value of [ ...nils, ...nonVoidPrims ])
				for (const form of getBothForms(<any> value, { kind: "composite-non-null" }))
					expect(form).to.throw(reInvalid);
		});

		// composite-non-void
		it('should validate non-void composites', () => {
			for (const value of [ ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "composite-non-void" }))
					expect(form).to.not.throw();

			for (const value of [ ...nonVoidPrims, ...nils, ...undefs ])
				for (const form of getBothForms(<any> value, { kind: "composite-non-void" }))
					expect(form).to.throw(reInvalid);
		});

		// defined
		it('should validate anything except `undefined`', () => {
			for (const value of [ ...nils, ...nonVoidPrims, ...nonVoidObjs ])
				for (const form of getBothForms(<any> value, { kind: "defined" }))
					expect(form).to.not.throw();

			for (const value of [ ...undefs ])
				for (const form of getBothForms(<any> value, { kind: "defined" }))
					expect(form).to.throw(reInvalid);
		});
	});

	describe('validator "custom"', () => {
		it("should apply custom constraint to values", () => {
			type Avenger = { hero: string; };

			const stark: Avenger = { hero: "Iron Man" };
			const steve: Avenger = { hero: "Captain America" };
			const quill: object = { name: "Peter Quill" };

			const isHero = (name: string) => ({ hero: actual }: Avenger) => name === actual;

			const isAvenger = {
				"is not an avenger": (person: object) => typeof Object(person).hero === "string",
			};

			const isIronMan = {
				...isAvenger,
				"is not an Iron Man": isHero("Iron Man"),
			};

			const isCap = {
				...isAvenger,
				"is not a Captain America": isHero("Captain America"),
			};

			for (const form of getBothForms(stark, isIronMan))
				expect(form).to.not.throw();

			for (const form of getBothForms(steve, isCap))
				expect(form).to.not.throw();

			for (const form of getBothForms(stark, isCap))
				expect(form).to.throw(/Validation failed: value is not a Captain America/);

			for (const form of getBothForms(steve, isIronMan))
				expect(form).to.throw(/Validation failed: value is not an Iron Man/);

			for (const form of getBothForms(quill, isAvenger))
				expect(form).to.throw(/Validation failed: value is not an avenger/);
		});
	});

	// ***

	describe('validator "typeOf"', () => {
		it("should validate values of a particular type", () => {
			for (const string of [ "Thor", "Ragnarok" ])
				for (const form of getBothForms(string, { typeOf: "string" }))
					expect(form).to.not.throw();

			for (const form of getBothForms(new String("Ass-guard"), { typeOf: "string" }))
				expect(form).to.throw(reInvalid);
		});
	});

	describe('validator "instanceOf"', () => {
		it("should validate instances of a particular class", () => {
			const Thingy = class {};
			const thingy = new Thingy();

			for (const form of getBothForms(thingy, { instanceOf: Thingy }))
				expect(form).to.not.throw();

			for (const object of [ [], {}, (() => {}) ])
				for (const form of getBothForms(object, { instanceOf: Thingy }))
					expect(form).to.throw(reInvalid);
		});
	});

	// ***

	describe('validator "number"', () => {
		// any
		it("should validate numbers", () => {
			for (const object of [ 17, 42, NaN, Infinity ])
				for (const form of getBothForms(object, { number: "any" }))
					expect(form).to.not.throw();

			for (const object of [ [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "any" }))
					expect(form).to.throw(reInvalid);
		});

		// numeric
		it("should validate any number, except NaN", () => {
			for (const object of [ 17, 42, Infinity ])
				for (const form of getBothForms(object, { number: "numeric" }))
					expect(form).to.not.throw();

			for (const object of [ [], {}, NaN, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "numeric" }))
					expect(form).to.throw(reInvalid);
		});

		// finite
		it("should validate finite numbers", () => {
			for (const object of [ 17, 42 ])
				for (const form of getBothForms(object, { number: "finite" }))
					expect(form).to.not.throw();

			for (const object of [ [], {}, Infinity, NaN, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "finite" }))
					expect(form).to.throw(reInvalid);
		});

		// non-integer
		it("should validate non-integer finite numbers", () => {
			for (const object of [ 17.42, 15.5, Math.PI, Infinity, NaN ])
				for (const form of getBothForms(object, { number: "non-integer" }))
					expect(form).to.not.throw();

			for (const object of [ 17, 42, 1.0, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "non-integer" }))
					expect(form).to.throw(reInvalid);
		});

		// non-integer-finite
		it("should validate non-integer finite numbers", () => {
			for (const object of [ 17.42, 15.5, Math.PI ])
				for (const form of getBothForms(object, { number: "non-integer-finite" }))
					expect(form).to.not.throw();

			for (const object of [ 17, 42, 1.0, Infinity, NaN, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "non-integer-finite" }))
					expect(form).to.throw(reInvalid);
		});

		// integer
		it("should validate integers", () => {
			for (const object of [ 17, 42, 1.0, Number.MAX_SAFE_INTEGER ])
				for (const form of getBothForms(object, { number: "integer" }))
					expect(form).to.not.throw();

			for (const object of [ 17.42, -Math.PI, Infinity, NaN, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "integer" }))
					expect(form).to.throw(reInvalid);
		});

		// integer-even
		it("should validate even integers", () => {
			for (const object of [ 42, 2, -1118 ])
				for (const form of getBothForms(object, { number: "integer-even" }))
					expect(form).to.not.throw();

			for (const object of [ 17, 1.0, -Math.PI, Infinity, NaN, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "integer-even" }))
					expect(form).to.throw(reInvalid);
		});

		// integer-odd
		it("should validate odd integers", () => {
			for (const object of [ 17, 1.0 ])
				for (const form of getBothForms(object, { number: "integer-odd" }))
					expect(form).to.not.throw();

			for (const object of [ 42, 2, -1118, -Math.PI, Infinity, NaN, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "integer-odd" }))
					expect(form).to.throw(reInvalid);
		});

		// integer-safe
		it("should validate safe integers", () => {
			for (const object of [ 17, 1.0, 42, 2, -1118, Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER ])
				for (const form of getBothForms(object, { number: "integer-safe" }))
					expect(form).to.not.throw();

			for (const object of [ Number.MAX_SAFE_INTEGER + 1, Number.MIN_SAFE_INTEGER - 1, Infinity, NaN, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "integer-safe" }))
					expect(form).to.throw(reInvalid);
		});

		// integer-even-safe
		it("should validate even safe integers", () => {
			for (const object of [ 42, 2, -1118, Number.MAX_SAFE_INTEGER - 1, Number.MIN_SAFE_INTEGER + 1 ])
				for (const form of getBothForms(object, { number: "integer-even-safe" }))
					expect(form).to.not.throw();

			for (const object of [ Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER, Infinity, NaN, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "integer-even-safe" }))
					expect(form).to.throw(reInvalid);
		});

		// integer-odd-safe
		it("should validate odd safe integers", () => {
			for (const object of [ 17, 1.0, Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER ])
				for (const form of getBothForms(object, { number: "integer-odd-safe" }))
					expect(form).to.not.throw();

			for (const object of [ Infinity, NaN, [], {}, "text", /regex/ ])
				for (const form of getBothForms(object, { number: "integer-odd-safe" }))
					expect(form).to.throw(reInvalid);
		});
	});

	describe('validator "spectrum"', () => {
		it("should validate numbers from a particular part of number spectrum", () => {
			for (const number of [ -42, -17, -3.5, -Infinity ])
				for (const spectrum of [ "negative", "non-positive" ])
					for (const form of getBothForms(number, <any> { spectrum }))
						expect(form).to.not.throw();

			for (const zero of [ 0, +0, -0 ])
				for (const form of getBothForms(zero, { spectrum: "non-zero" }))
					expect(form).to.throw(reInvalid);
		});
	});

	describe('validator "range"', () => {
		it("should validate numbers within a particular range", () => {
			const getRange = () => <any> [[ 42, -17 ], [ -17, 42 ]][Math.floor(Math.random() * 2)];

			for (const number of [ 42, -17, 0 ])
				for (const form of getBothForms(number, { range: getRange() }))
					expect(form).to.not.throw();

			for (const number of [ -100, Infinity, NaN ])
				for (const form of getBothForms(number, { range: getRange() }))
					expect(form).to.throw(reInvalid);
		});
	});

	// ***

	describe('validator "pattern"', () => {
		it("should validate strings, that match a particular pattern", () => {
			const phone = /\d{3}-\d{4}/;

			for (const form of getBothForms("555-0123", { pattern: phone }))
				expect(form).to.not.throw();

			for (const string of [ "Bruce", "Almighty" ])
				for (const form of getBothForms(string, { pattern: phone }))
					expect(form).to.throw(reInvalid);
		});
	});

	describe('validator "length"', () => {
		it("should validate particularly long strings", () => {
			for (const form of getBothForms("1234567890", { length: 10 }))
				expect(form).to.not.throw();

			for (const form of getBothForms("hello", { length: 10 }))
				expect(form).to.throw(reInvalid);
		});
	});

	describe('validator "lengthRange"', () => {
		it("should validate strings, which length is within the bounds", () => {
			const short = "hi";
			const long = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";

			for (const form of getBothForms(short, { lengthRange: [ 0, 10 ] }))
				expect(form).to.not.throw();

			for (const form of getBothForms(long, { lengthRange: [ 0, 10 ] }))
				expect(form).to.throw(reInvalid);
		});
	});
});

describe('Complex validations', () => {
	it("validation of natural numbers", () => {
		const natural: any = { number: "integer", spectrum: "non-negative" };

		for (const number of [ 0, 1, 2, 3, 15, 17, 42, 100, 1e3 ])
			for (const form of getBothForms(number, natural))
				expect(form).to.not.throw();

		for (const object of [ -1, -17, -42, -100, NaN, Infinity, null, {} ])
			for (const form of getBothForms(<any> object, natural))
				expect(form).to.throw(reInvalid);
	});
});

describe('Using standalone validations', () => {
	it("accepts known strings", () => {
		expect(() => valur(17).as("negative", "integer")).to.throw(reInvalid);
		expect(() => valur(17).as("positive", "integer")).to.not.throw();
	});

	it('accepts functions', () => {
		expect(() => valur(42).as(() => false)).to.throw(/does not meet the constraint:/);
		expect(() => valur(42).as(() => false, () => false)).to.throw(/does not meet the constraint \[0\]:/);
		expect(() => valur(42).as(() => true, () => false)).to.throw(/does not meet the constraint \[1\]:/);

		expect(() => valur(42).as(() => true)).to.not.throw();
	});

	it('accepts regular expressions', () => {
		expect(() => valur("text").as(/string/)).to.throw(reInvalid);
		expect(() => valur(42).as(/42/)).to.throw(reInvalid);

		expect(() => valur("text").as(/text/)).to.not.throw();
	});

	it('accepts values of other types', () => {
		expect(() => valur(42).as(17)).to.throw(reInvalid);
		expect(() => valur(42).as(13, 15)).to.throw(reInvalid);
		expect(() => valur("text").as("string")).to.throw(reInvalid);
		expect(() => valur({ prop: null }).as({ prop: null })).to.throw(reInvalid);

		const obj: any = { prop: null };

		expect(() => valur(42).as(42)).to.not.throw();
		expect(() => valur(42).as(42, 17)).to.not.throw();
		expect(() => valur("text").as("text")).to.not.throw();
		expect(() => valur(obj).as(obj)).to.not.throw();
	});
});

describe('Using config', () => {
	describe('modifier "mode"', () => {
		const value: number = 17;
		const descriptor: any = { range: [ 1, 10 ] };

		it("should break runtime on validation failure", () => {
			expect(() => valur(value).as(descriptor, { mode: "assert" })).to.throw(reInvalid);
		});

		it("should return `false` on validation failure", () => {
			expect(valur(value).as(descriptor, { mode: "check" })).to.be.false;
		});

		it("should return object containing validation results", () => {
			expect(valur(value, "age").as(descriptor, { mode: "describe" }))
				.to.deep.equal({ range: 'age is out of bounds "[1 to 10]"' });
		});

		it("should return results of the first failed validation (if any)", () => {
			expect(valur(value, "age").as(descriptor, { mode: "switch" }))
				.to.deep.equal({ name: 'range', value: 17 });
		});
	});

	describe('modifier "checkStrict"', () => {
		const value: number = -15;
		const descriptor: any = { spectrum: "positive", number: "integer" };

		it("should validate values with at least one validation successfuly passed", () => {
			expect(valur(value).as(descriptor, { mode: "check", checkStrict: false })).to.be.true;
		});

		it("should force all validation to be successfuly passed", () => {
			expect(valur(value).as(descriptor, { mode: "check", checkStrict: true })).to.be.false;
		});
	});
});
