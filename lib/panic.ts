import { is } from "@valuer/is";

export namespace panic {
	export type Kind = "Invalid validator" | "Invalid validation" | "Invalid config" | "Validation failed" | "Unexpected error";
	export type Text = string;

	/**
	 * Throw error
	 * @arg kind Kind of error
	 * @arg text Error message
	 * @arg details (optional) Additional error details
	 */
	export const now = (kind: Kind, text: Text, ...details: string[]): never => {
		const message = [ kind, text, details.join(", ") || null ].filter(is._nonVoid).join(": ");

		if (kind === "Validation failed")
			null; // TODO: #23

		throw new Error(message);
	};
}
